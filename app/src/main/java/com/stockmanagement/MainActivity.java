package com.stockmanagement;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import com.stockmanagement.data.LoginActivity;
import com.stockmanagement.data.purchase.PurchaseMain;
import com.stockmanagement.data.utils.PreferenceUtils;

public class MainActivity extends AppCompatActivity {


    private ImageView ivSplashImage;
    private Intent mainIntent;
    private boolean isRegistered;
    PreferenceUtils pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pref = new PreferenceUtils(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (pref.getLogin().equals("1")) {

                    Intent intent;
                    intent = new Intent(MainActivity.this, PurchaseMain.class);
                    startActivity(intent);
                    finish();

                }else {

                    Intent intent;
                    intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }



            }
        }, 3000);

    }
}
