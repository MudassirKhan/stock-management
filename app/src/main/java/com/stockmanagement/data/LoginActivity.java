package com.stockmanagement.data;

import android.content.Intent;
import android.database.Cursor;
import android.opengl.ETC1;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.stockmanagement.MainActivity;
import com.stockmanagement.R;
import com.stockmanagement.data.database.DatabaseHelper;
import com.stockmanagement.data.purchase.PurchaseMain;
import com.stockmanagement.data.utils.PreferenceUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.widget.Toast.LENGTH_SHORT;

public class LoginActivity extends AppCompatActivity {

    EditText MOBILE_NUM,PASSWORD ;
    Button LOGIN ;
    TextView REGISTER_LINK;

    PreferenceUtils pref;

    DatabaseHelper dbhelper;

    String[] partyNameList = new String[]{"Select Blood Group",
            "Mudassir",
            "Mujahid",
            "Danish",
            "Zubair",
            "Faisal",
            "Saad",
            "Mannan",
            "Abdullah"
    };

    String[] productList = new String[]{"Select Blood Group",
            "A-Nali",
            "Dobban",
            "Jonson",
            "R . Bulling",
            "S . Bulling",
            "P . Bulling",
            "K . P . P ",
            "N . P . P",
            "W . P . P",
            "R . P . P",
            "N . K ",
            "Jain",
            "R - Pipe"

    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        MOBILE_NUM = findViewById(R.id.ettEmail);
        PASSWORD = findViewById(R.id.edttPass);
        LOGIN = findViewById(R.id.btnLogin);
        REGISTER_LINK = findViewById(R.id.txtAreYouPract);

        pref = new PreferenceUtils(this);
        dbhelper = new DatabaseHelper(this);

        REGISTER_LINK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(LoginActivity.this,RegistrationActivity.class);
                startActivity(intent);
                finish();
            }

        });

        LOGIN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginAccount();
            }
        });
     //   Boolean result = dbhelper.insertProduct(partyNameList);



    }

    private void loginAccount() {

        String numberString = MOBILE_NUM.getText().toString();
        String passworString = PASSWORD.getText().toString();

            if(numberString.length() > 10 || numberString.length() < 10){

                MOBILE_NUM.setError("Incorrect Phone Number");
                MOBILE_NUM.requestFocus();
                Toast.makeText(LoginActivity.this, "Invalid Number", Toast.LENGTH_LONG).show();
                return;
            }else {
                char c = numberString.charAt(0);
                if(c == '7' || c == '8' || c == '9'){
                    if(passworString.isEmpty()){
                        PASSWORD.setError("Password required");
                        PASSWORD.requestFocus();
                        return;
                    }
                    if(passworString.length() < 6){
                        PASSWORD.setError("Password should be atleast 6 character long");
                        PASSWORD.requestFocus();
                        return;
                    }

                        //do something here

                    Cursor res = dbhelper.getAllData(PASSWORD.getText().toString().trim());
                    if (res != null && res.getCount() > 0) {
                        Toast.makeText(this, "Login", LENGTH_SHORT).show();

                        pref.setLogin("1");
                        Intent intent=new Intent(LoginActivity.this, PurchaseMain.class);
                        startActivity(intent);
                        finish();

                    } else {
                        Toast.makeText(this, "User Not Matched", LENGTH_SHORT).show();
                    }

                }else {
                    Toast.makeText(LoginActivity.this, "Enter Valid Number", Toast.LENGTH_LONG).show();
                }

            }
    }

}
