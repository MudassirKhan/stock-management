package com.stockmanagement.data;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.stockmanagement.R;
import com.stockmanagement.data.database.DatabaseHelper;
import com.stockmanagement.data.purchase.PurchaseMain;

import static android.widget.Toast.LENGTH_SHORT;

public class RegistrationActivity extends AppCompatActivity {

    EditText MOBILE_NUM,PASSWORD ,USERNAME;
    Button SINGUP ;
    TextView LOGIN_LINK;
    DatabaseHelper dbhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);

        USERNAME = findViewById(R.id.username);
        PASSWORD = findViewById(R.id.password);
        MOBILE_NUM = findViewById(R.id.mobile);
        SINGUP = findViewById(R.id.singup);
        LOGIN_LINK = findViewById(R.id.signin);

        dbhelper = new DatabaseHelper(this);

        LOGIN_LINK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(RegistrationActivity.this,LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
        SINGUP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                singupAccount();
            }
        });



    }

    private void singupAccount() {

        String usernameString = USERNAME.getText().toString();
        String numberString = MOBILE_NUM.getText().toString();
        String passworString = PASSWORD.getText().toString();

        if(usernameString.isEmpty()){
            USERNAME.setError("Username required");
            USERNAME.requestFocus();
            return;
        }

        if(numberString.length() > 10 || numberString.length() < 10){

            MOBILE_NUM.setError("Incorrect Phone Number");
            MOBILE_NUM.requestFocus();

            Toast.makeText(RegistrationActivity.this, "Invalid Number", Toast.LENGTH_LONG).show();
            return;
        }else {
            char c = numberString.charAt(0);
            if(c == '7' || c == '8' || c == '9'){

                if(passworString.isEmpty()){
                    PASSWORD.setError("Password required");
                    PASSWORD.requestFocus();
                    return;
                }
                if(passworString.length() < 6){
                    PASSWORD.setError("Password should be atleast 6 character long");
                    PASSWORD.requestFocus();
                    return;
                }

                //do something here

                if(! MOBILE_NUM.getText().toString().equals("")){

                    Cursor res = dbhelper.checkUSer(MOBILE_NUM.getText().toString().trim());
                    if (res != null && res.getCount() > 0) {

                        MOBILE_NUM.setText("");
                        PASSWORD.setText("");

                        Toast.makeText(this, "User Exist...Use Other Mobile Number", LENGTH_SHORT).show();
                    } else {

                        Boolean result = dbhelper.insertRegisterData(usernameString,numberString,passworString);

                        if (result == true) {


                            Toast.makeText(RegistrationActivity.this, "Register Successfully", Toast.LENGTH_SHORT).show();

                            Intent intent=new Intent(RegistrationActivity.this,LoginActivity.class);
                            startActivity(intent);
                            finish();

                        } else {

                            Toast.makeText(RegistrationActivity.this, "Try Again", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

            }else {
                Toast.makeText(RegistrationActivity.this, "Enter Valid Number", Toast.LENGTH_LONG).show();
            }

        }


    }
}
