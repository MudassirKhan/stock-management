package com.stockmanagement.data;

import java.text.SimpleDateFormat;

public class Utils {

    public static String convertDate(String dateInMilliseconds) {
        String time24 = "";
        try {
            SimpleDateFormat inFormat = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd");
            time24 = outFormat.format(inFormat.parse(dateInMilliseconds));
            System.out.println("time in 24 hour format : " + time24);
        } catch (Exception e) {
            System.out.println("Exception : " + e.getMessage());
        }

        return time24.toString();
    }
}
