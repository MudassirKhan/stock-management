package com.stockmanagement.data.database;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.stockmanagement.R;
import com.stockmanagement.data.net.ModelNet;
import com.stockmanagement.data.purchase.ModelPurchase;
import com.stockmanagement.data.sold.ModelSold;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.content.ContentValues.TAG;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Details.db";

    public static final String TABLE_REGISTER = "TABLE_REGISTER";
    public static final String TABLE_PURCHASE = "TABLE_PURCHASE";
    public static final String TABLE_SOLD = "TABLE_SOLD";
    public static final String TABLE_NET = "TABLE_NET";
    public static final String TABLE_PRODUCT = "TABLE_PRODUCT";

    public static final String COL_PRODUCT_ID = "COL_PRODUCT_ID";
    public static final String PRODUCT_NAME = "PRODUCT_NAME";

    public static final String COL_R_ID = "R_ID";
    public static final String COL_1_NAME = "NAME";
    public static final String COL_2_MOBILE = "MOBILE";
    public static final String COL_3_PASSWORD = "PASSWORD";

    public static final String P_ID = "P_ID";
    public static final String COL_1_PCUSTOMERNAME = "P_NAME";
    public static final String COL_2_PPRODUCT= "P_PRODUCT";
    public static final String COL_3_PDATE = "P_DATE";
    public static final String COL_4_PWEIGHT = "P_WEIGHT";
    public static final String COL_5_PBAGS = "P_BAGS";

    public static final String S_ID = "S_ID";
    public static final String COL_1_SCUSTOMERNAME ="S_NAME";
    public static final String COL_2_SPRODUCT= "S_PRODUCT";
    public static final String COL_3_SDATE = "S_DATE";
    public static final String COL_4_SWEIGHT = "S_WEIGHT";
    public static final String COL_5_SBAGS = "S_BAGS";

    public static final String N_ID = "N_ID";
    public static final String COL_1_NET_PRODUCT ="NET_PRODUCT";
    public static final String COL_2_NET_WEIGHT = "NET_WEIGHT";
    public static final String COL_2_NET_BAGS = "NET_BAG";

    public DatabaseHelper(Context context)
    {
        super(context,DATABASE_NAME, null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE " + TABLE_REGISTER +"(ID INTEGER PRIMARY KEY AUTOINCREMENT,NAME VARCHAR,MOBILE INTEGER,PASSWORD VARCHAR)");
        db.execSQL("CREATE TABLE " + TABLE_PURCHASE +"(ID INTEGER PRIMARY KEY AUTOINCREMENT,P_NAME VARCHAR,P_PRODUCT VARCHAR,P_DATE VARCHAR,P_WEIGHT VARCHAR,P_BAGS INTEGER)");
        db.execSQL("CREATE TABLE " + TABLE_SOLD +"(ID INTEGER PRIMARY KEY AUTOINCREMENT,S_NAME VARCHAR,S_PRODUCT VARCHAR,S_DATE VARCHAR,S_WEIGHT INTEGER,S_BAGS INTEGER)");
        db.execSQL("CREATE TABLE " + TABLE_NET +"(ID INTEGER PRIMARY KEY AUTOINCREMENT,NET_PRODUCT VARCHAR,NET_WEIGHT VARCHAR,NET_BAG VARCHAR)");
        db.execSQL("CREATE TABLE " + TABLE_PRODUCT +"(COL_PRODUCT_ID INTEGER PRIMARY KEY AUTOINCREMENT,PRODUCT_NAME VARCHAR)");

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

        db.execSQL( "DROP TABLE IF EXISTS"+TABLE_REGISTER);
        db.execSQL( "DROP TABLE IF EXISTS"+TABLE_PURCHASE);
        db.execSQL( "DROP TABLE IF EXISTS"+TABLE_SOLD);
        db.execSQL( "DROP TABLE IF EXISTS"+TABLE_NET);
        db.execSQL( "DROP TABLE IF EXISTS"+TABLE_PRODUCT);

    }

   /* public boolean insertProduct(String[] partyNameList) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues _Values = new ContentValues();
        Activity fContext = null;
        long result = 0;

        List<String> MathFormulas = Arrays.asList(fContext.getResources().getStringArray(R.array.mytable_records_v1));
        for (int i = 0; i < MathFormulas.size(); i++) {
            _Values.put(PRODUCT_NAME, MathFormulas.get(i));

            result = db.insert(TABLE_PRODUCT,null,_Values);
        }

        Log.e("INSERTED_PRODUCT" , String.valueOf(_Values));

        if(result == -1)
        {
            return  false;
        }
        else
        {
            return  true;
        }

    }*/


    public boolean insertRegisterData(String name,String mobile,String password) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_1_NAME,name);
        contentValues.put(COL_2_MOBILE,mobile);
        contentValues.put(COL_3_PASSWORD,password);

        long result = db.insert(TABLE_REGISTER,null,contentValues);

        if(result == -1)
        {
            return  false;
        }
        else
        {
            return  true;
        }

    }

    public boolean insertPurchaseData(ModelPurchase modelPurchase) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_1_PCUSTOMERNAME, modelPurchase.getP_partyname());
        contentValues.put(COL_2_PPRODUCT, modelPurchase.getP_name());
        contentValues.put(COL_3_PDATE, modelPurchase.getP_date());
        contentValues.put(COL_4_PWEIGHT, modelPurchase.getP_weight());
        contentValues.put(COL_5_PBAGS, modelPurchase.getP_kgorbags());

        long result = db.insert(TABLE_PURCHASE,null,contentValues);

        if(result == -1)
        {
            return  false;
        }
        else
        {
            return  true;
        }

    }

    public boolean insertSoldData(ModelSold modelPurchase) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_1_SCUSTOMERNAME, modelPurchase.getP_partyname());
        contentValues.put(COL_2_SPRODUCT, modelPurchase.getP_name());
        contentValues.put(COL_3_SDATE, modelPurchase.getP_date());
        contentValues.put(COL_4_SWEIGHT, modelPurchase.getP_weight());
        contentValues.put(COL_5_SBAGS, modelPurchase.getP_kgorbags());

        long result = db.insert(TABLE_SOLD,null,contentValues);

        if(result == -1)
        {
            return  false;
        }
        else
        {
            return  true;
        }

    }

    public boolean insertNetData(ModelNet modelNet) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_1_NET_PRODUCT, modelNet.getP_name());
        contentValues.put(COL_2_NET_WEIGHT,modelNet.getP_weight());
       // contentValues.put(COL_2_NET_BAGS, modelNet.getP_bagse());

        long result = db.insert(TABLE_NET,null,contentValues);

        Log.e("Testbugsss", String.valueOf(contentValues));

        if(result == -1)
        {
            return  false;
        }
        else
        {
            return  true;
        }

    }

    public Cursor getAllData(String pass) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery( "SELECT * FROM "+ TABLE_REGISTER + " Where PASSWORD = '"+pass+"'",null);
        return  res;
    }

    public Cursor checkUSer(String mobile) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery( "SELECT * FROM "+ TABLE_REGISTER + " Where MOBILE = '"+mobile+"'",null);
        return  res;
    }

    public List<ModelPurchase> getAllUser() {

        List<ModelPurchase> usersdetail = new ArrayList<>();

        String USER_DETAIL_SELECT_QUERY = "SELECT * FROM " + TABLE_PURCHASE;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(USER_DETAIL_SELECT_QUERY, null);

        try {
            if (cursor.moveToFirst()) {
                do {
                    ModelPurchase userData = new ModelPurchase("","","","","");
                    userData.setP_partyname(cursor.getString(cursor.getColumnIndex(COL_1_PCUSTOMERNAME)));
                    userData.setP_name(cursor.getString(cursor.getColumnIndex(COL_2_PPRODUCT)));
                    userData.setP_date(cursor.getString(cursor.getColumnIndex(COL_3_PDATE)));
                    userData.setP_weight(cursor.getString(cursor.getColumnIndex(COL_4_PWEIGHT)));
                    userData.setP_kgorbags(cursor.getString(cursor.getColumnIndex(COL_5_PBAGS)));


                    usersdetail.add(userData);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to get posts from database");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return usersdetail;

    }

    public List<ModelSold> getAllSoldUser() {

        List<ModelSold> usersdetail = new ArrayList<>();

        String USER_SOLDDETAIL_SELECT_QUERY = "SELECT * FROM " + TABLE_SOLD;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(USER_SOLDDETAIL_SELECT_QUERY, null);

        try {
            if (cursor.moveToFirst()) {
                do {

                    ModelSold userData = new ModelSold("","","","","");
                    userData.setP_partyname(cursor.getString(cursor.getColumnIndex(COL_1_SCUSTOMERNAME)));
                    userData.setP_name(cursor.getString(cursor.getColumnIndex(COL_2_SPRODUCT)));
                    userData.setP_date(cursor.getString(cursor.getColumnIndex(COL_3_SDATE)));
                    userData.setP_weight(cursor.getString(cursor.getColumnIndex(COL_4_SWEIGHT)));
                    userData.setP_kgorbags(cursor.getString(cursor.getColumnIndex(COL_5_SBAGS)));

                    usersdetail.add(userData);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to get posts from database");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return usersdetail;

    }

    public List<ModelNet> getAllNet() {

        List<ModelNet> usersdetail = new ArrayList<>();

        String USER_SOLDDETAIL_SELECT_QUERY = "SELECT * FROM " + TABLE_NET;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(USER_SOLDDETAIL_SELECT_QUERY, null);

        try {
            if (cursor.moveToFirst()) {
                do {

                    ModelNet userData = new ModelNet("","","");

                    userData.setP_name(cursor.getString(cursor.getColumnIndex(COL_1_NET_PRODUCT)));
                    userData.setP_weight(cursor.getString(cursor.getColumnIndex(COL_2_NET_WEIGHT)));
                   // userData.setP_bagse(cursor.getString(cursor.getColumnIndex(COL_2_NET_BAGS)));

                    usersdetail.add(userData);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to get posts from database");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return usersdetail;

    }

    public List<ModelNet> getAllNetForPS(String productname) {

        List<ModelNet> usersdetail = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery( "SELECT * FROM "+ TABLE_NET + " Where NET_PRODUCT = '"+productname+"'",null);

        try {
            if (cursor.moveToFirst()) {
                do {

                    ModelNet userData = new ModelNet("","","");

                    userData.setP_name(cursor.getString(cursor.getColumnIndex(COL_1_NET_PRODUCT)));
                    userData.setP_weight(cursor.getString(cursor.getColumnIndex(COL_2_NET_WEIGHT)));
                   // userData.setP_bagse(cursor.getString(cursor.getColumnIndex(COL_2_NET_BAGS)));

                    usersdetail.add(userData);

                } while (cursor.moveToNext());
            }


        } catch (Exception e) {
            Log.d(TAG, "Error while trying to get posts from database");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return usersdetail;

    }

    public boolean updateAllNetForPS(String pname , String pkg , String bags) {

        long result = 0;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NET + " Where NET_PRODUCT = '" + pname + "'", null);

        if (cursor.moveToFirst()) {

            db.execSQL("UPDATE TABLE_NET SET NET_WEIGHT='" + pkg + "' WHERE NET_PRODUCT='" + pname + "'");

        } else {

        }

        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public boolean updateChangePassword(String mobile , String password) {

        long result = 0;
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_REGISTER + " Where MOBILE = '" + mobile + "'", null);

        if (cursor.moveToFirst()) {

            db.execSQL("UPDATE TABLE_REGISTER SET PASSWORD='" + password + "' WHERE MOBILE='" + mobile + "'");

        }

        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }
}

