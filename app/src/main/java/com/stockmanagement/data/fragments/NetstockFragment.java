package com.stockmanagement.data.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.stockmanagement.R;
import com.stockmanagement.data.database.DatabaseHelper;
import com.stockmanagement.data.net.ModelNet;
import com.stockmanagement.data.net.NetAdapter;
import com.stockmanagement.data.sold.ModelSold;
import com.stockmanagement.data.sold.SoldAdapter;

import java.util.ArrayList;

public class NetstockFragment extends Fragment {

    RecyclerView recyclerView;
    DatabaseHelper dbHelper;
    NetAdapter adapter;

    ArrayList<ModelNet> list = new ArrayList<>();

    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        dbHelper = new DatabaseHelper(getActivity());
        recyclerView = view.findViewById(R.id.doctrList);

        list =  (ArrayList<ModelNet>) dbHelper.getAllNet();

        GridLayoutManager gLayoutManager = new GridLayoutManager(getActivity(), 1); // (Context context, int spanCount)
        recyclerView.setLayoutManager(gLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = new NetAdapter(getActivity() ,list);
        recyclerView.setAdapter(adapter);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        try {
            return inflater.inflate(R.layout.netfragment, container, false);
        } catch (Exception e) {
        }
        return super.onCreateView(inflater, container, savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

        adapter = new NetAdapter(getActivity() ,(ArrayList<ModelNet>) dbHelper.getAllNet());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

    }
}
