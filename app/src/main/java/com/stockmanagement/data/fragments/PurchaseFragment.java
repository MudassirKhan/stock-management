package com.stockmanagement.data.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.stockmanagement.R;
import com.stockmanagement.data.LoginActivity;
import com.stockmanagement.data.RegistrationActivity;
import com.stockmanagement.data.database.DatabaseHelper;
import com.stockmanagement.data.purchase.AddPurchase;
import com.stockmanagement.data.purchase.ModelPurchase;
import com.stockmanagement.data.purchase.PurchaseAdapter;

import java.util.ArrayList;
import java.util.Collections;

public class PurchaseFragment extends Fragment {

    FloatingActionButton fab;
    RecyclerView recyclerView;
    DatabaseHelper dbHelper;
    PurchaseAdapter adapter;

    ArrayList<ModelPurchase> list = new ArrayList<>();
    ArrayList<ModelPurchase> list2 = new ArrayList<>();

    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        fab = view.findViewById(R.id.fab);
        dbHelper = new DatabaseHelper(getActivity());
        recyclerView = view.findViewById(R.id.recyclerView);

        list.clear();
        list2.clear();
/*
        list =  (ArrayList<ModelPurchase>) dbHelper.getAllUser();
        list2 =list;
        Collections.reverse(list2);*/

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        adapter = new PurchaseAdapter(getActivity() ,list);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

/*
        GridLayoutManager gLayoutManager = new GridLayoutManager(getActivity(), 1); // (Context context, int spanCount)
        recyclerView.setLayoutManager(gLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = new PurchaseAdapter(getActivity() ,list);
        recyclerView.setAdapter(adapter);*/

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), AddPurchase.class);
                startActivity(intent);

            }

        });

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        try {
            return inflater.inflate(R.layout.purchasefragment, container, false);
        } catch (Exception e) {
        }
        return super.onCreateView(inflater, container, savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

        adapter = new PurchaseAdapter(getActivity() , (ArrayList<ModelPurchase>) dbHelper.getAllUser());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

    }
}
