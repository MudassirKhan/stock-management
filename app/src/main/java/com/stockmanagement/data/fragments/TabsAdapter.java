package com.stockmanagement.data.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class TabsAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;
    Intent i = null;

    Bundle bundle = new Bundle();

    public TabsAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.mNumOfTabs = tabCount;

    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                PurchaseFragment info = new PurchaseFragment();
                info.setArguments(bundle);
                return info;

            case 1:
                SoldFragment address = new SoldFragment();
                address.setArguments(bundle);
                return address;

            case 2:
                NetstockFragment openhourse = new NetstockFragment();
                openhourse.setArguments(bundle);
                return openhourse;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
