package com.stockmanagement.data.net;

public class ModelNet {

    public String p_name;
    public String p_bagse;

    public String getP_name() {
        return p_name;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name;
    }

    public String getP_bagse() {
        return p_bagse;
    }

    public void setP_bagse(String p_bagse) {
        this.p_bagse = p_bagse;
    }

    public String getP_weight() {
        return p_weight;
    }

    public void setP_weight(String p_weight) {
        this.p_weight = p_weight;
    }

    public String p_weight;

    public ModelNet(String p_name, String p_bagse, String p_weight) {
        this.p_name = p_name;
        this.p_bagse = p_bagse;
        this.p_weight = p_weight;
    }
}
