package com.stockmanagement.data.net;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.stockmanagement.R;
import com.stockmanagement.data.database.DatabaseHelper;
import com.stockmanagement.data.purchase.ModelPurchase;
import com.stockmanagement.data.utils.PreferenceUtils;

import java.util.ArrayList;

public class NetAdapter extends RecyclerView.Adapter<NetAdapter.ViewHolder>  {

    Activity mContext;
    Context context;
    View itemLayoutView;
    ArrayList<ModelNet> arrayList =  new ArrayList<>();
    PreferenceUtils pref;
    DatabaseHelper db;

    public NetAdapter(Activity mContext, ArrayList<ModelNet> arrayList) {
        this.mContext = mContext;
        this.context = context;
        this.arrayList = arrayList;
        pref = new PreferenceUtils(mContext);
        db = new DatabaseHelper(mContext);

    }


    @NonNull
    @Override
    public NetAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.net_row, parent, false);
        NetAdapter.ViewHolder viewHolder = new NetAdapter.ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull NetAdapter.ViewHolder holder, int position) {

        holder.productName.setText(arrayList.get(position).getP_name());
        holder.productKg.setText(arrayList.get(position).getP_weight());
        holder.productbags.setText(" Kg / Bags ");


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView productName ,productKg,productbags;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            productName = itemLayoutView.findViewById(R.id.productName);
            productKg = itemLayoutView.findViewById(R.id.productKg);
            productbags = itemLayoutView.findViewById(R.id.productbags);


        }
    }
}
