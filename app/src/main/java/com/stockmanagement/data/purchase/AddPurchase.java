package com.stockmanagement.data.purchase;

import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.VisibleForTesting;
import androidx.appcompat.app.AppCompatActivity;

import com.stockmanagement.R;
import com.stockmanagement.data.Utils;
import com.stockmanagement.data.database.DatabaseHelper;
import com.stockmanagement.data.net.ModelNet;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import static android.widget.Toast.LENGTH_SHORT;

public class AddPurchase extends AppCompatActivity implements com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener {

    LinearLayout back;
    Spinner spCustomer,spproductName;
    RadioGroup radioOrderType;
    RadioButton kg, bags;
    EditText edtweight;
    TextView edtdate ,txtkgorbags;
    String partyName = "" , productName = "";
    String kgOrbags = "kg";
    Button btnpurchase;

    SimpleDateFormat simpleDateFormat;
    DatabaseHelper dbhelper;

    String[] partyNameList = new String[]{"Select Blood Group",
            "Mudassir",
            "Mujahid",
            "Danish",
            "Zubair",
            "Faisal",
            "Saad",
            "Mannan",
            "Abdullah"
    };

    String[] productList = new String[]{"Select Blood Group",
            "A-Nali",
            "Dobban",
            "Jonson",
            "R . Bulling",
            "S . Bulling",
            "P . Bulling",
            "K . P . P ",
            "N . P . P",
            "W . P . P",
            "R . P . P",
            "N . K ",
            "Jain",
            "R - Pipe"

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addpurchase);

        back = findViewById(R.id.back);
        spCustomer = findViewById(R.id.spCustomer);
        spproductName = findViewById(R.id.spproductName);
        radioOrderType = findViewById(R.id.radioOrderType);
        kg = findViewById(R.id.kg);
        bags = findViewById(R.id.bags);
        edtweight = findViewById(R.id.edtweight);
        edtdate = findViewById(R.id.edtdate);
        txtkgorbags = findViewById(R.id.txtkgorbags);
        btnpurchase = findViewById(R.id.btnpurchase);

        dbhelper = new DatabaseHelper(AddPurchase.this);

        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        radioOrderType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.kg) {
                    txtkgorbags.setText("Kg");
                    kgOrbags = "kg";
                } else if (i == R.id.bags) {
                    txtkgorbags.setText("Bags");
                    kgOrbags = "bags";
                }
            }
        });

        ArrayAdapter<String>  bloodAdapter = new ArrayAdapter<String>(AddPurchase.this, R.layout.spinner_item, partyNameList);
        bloodAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spCustomer.setAdapter(bloodAdapter);

        spCustomer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                partyName = spCustomer.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> ProductName = new ArrayAdapter<String>(AddPurchase.this, R.layout.spinner_item, productList);
        ProductName.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spproductName.setAdapter(ProductName);

        spproductName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                productName = spproductName.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        edtdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDate(2019, 0, 1, R.style.DatePickerSpinner);
            }
        });

        btnpurchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                addPurchaseProduct();
            }
        });


    }

    private void addPurchaseProduct() {

        int kg = 0;
        int bags = 0;
        Boolean resultupdate;

        String str_date = edtdate.getText().toString();
        String str_weight = edtweight.getText().toString();

        if(str_date.equals("") || str_weight.equals("")|| partyName.equals("") || productName.equals("")){

            Toast.makeText(this, "All Fields Required", LENGTH_SHORT).show();
        }else {

            ModelPurchase modelPurchase = new ModelPurchase(productName,partyName,str_weight,str_date,kgOrbags);

            modelPurchase.p_name = productName;
            modelPurchase.p_partyname = partyName;
            modelPurchase.p_weight = str_weight;
            modelPurchase.p_date = str_date;
            modelPurchase.p_kgorbags = kgOrbags;

            Boolean result = dbhelper.insertPurchaseData(modelPurchase);

            if (result == true) {

                ArrayList<ModelNet> list = (ArrayList<ModelNet>) dbhelper.getAllNetForPS(productName);

                if(list.size() != 0){

                    String productname =list.get(0).getP_name();
                    String productkg   =  list.get(0).getP_weight();

                    kg = Integer.valueOf(productkg);

                    int totalnet = Integer.valueOf(str_weight);

                    totalnet = kg + totalnet;
                    totalnet = bags + totalnet;

                    resultupdate = dbhelper.updateAllNetForPS(productname, String.valueOf(totalnet), String.valueOf(totalnet));


                }else {

                    ModelNet modelPurchas1e = new ModelNet("","","");

                    modelPurchas1e.p_name = productName;
                    modelPurchas1e.p_weight = str_weight;
                    Boolean resultifNU = dbhelper.insertNetData(modelPurchas1e);

                }



                partyName = "";
                productName = "";
                edtdate.setText("");
                edtweight.setText("");

                Toast.makeText(AddPurchase.this, "Purchase Data Saved Successfully", Toast.LENGTH_SHORT).show();
                Log.e("Stringtesteing" , modelPurchase.p_name = productName);
                Log.e("Stringtesteing" ,   modelPurchase.p_partyname = partyName);
                Log.e("Stringtesteing" ,  modelPurchase.p_date = str_date);
                Log.e("Stringtesteing" ,   modelPurchase.p_weight = str_weight);
                Log.e("Stringtesteing" ,  modelPurchase.p_kgorbags = kgOrbags);

                finish();

            } else {

                Toast.makeText(AddPurchase.this, "Try Again...", Toast.LENGTH_SHORT).show();
            }
        }


    }

    @VisibleForTesting
    void showDate(int year, int monthOfYear, int dayOfMonth, int spinnerTheme) {
        new SpinnerDatePickerDialogBuilder()
                .context(AddPurchase.this)
                .callback(AddPurchase.this)
                .spinnerTheme(spinnerTheme)
                .defaultDate(year, monthOfYear, dayOfMonth)
                .build()
                .show();
    }

    @Override
    public void onDateSet(com.tsongkha.spinnerdatepicker.DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
        edtdate.setText(Utils.convertDate(simpleDateFormat.format(calendar.getTime())));
    }
}
