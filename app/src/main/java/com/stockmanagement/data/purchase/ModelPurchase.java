package com.stockmanagement.data.purchase;

public class ModelPurchase {

    public String p_name;
    public String p_partyname;
    public String p_weight;
    public String p_date;
    public String p_kgorbags;

    public ModelPurchase(String p_name, String p_partyname, String p_weight, String p_date, String p_kgorbags) {
        this.p_name = p_name;
        this.p_partyname = p_partyname;
        this.p_weight = p_weight;
        this.p_date = p_date;
        this.p_kgorbags = p_kgorbags;
    }


    public String getP_weight() {
        return p_weight;
    }

    public void setP_weight(String p_weight) {
        this.p_weight = p_weight;
    }


    public String getP_name() {
        return p_name;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name;
    }

    public String getP_partyname() {
        return p_partyname;
    }

    public void setP_partyname(String p_partyname) {
        this.p_partyname = p_partyname;
    }



    public String getP_date() {
        return p_date;
    }

    public void setP_date(String p_date) {
        this.p_date = p_date;
    }

    public String getP_kgorbags() {
        return p_kgorbags;
    }

    public void setP_kgorbags(String p_kgorbags) {
        this.p_kgorbags = p_kgorbags;
    }
}
