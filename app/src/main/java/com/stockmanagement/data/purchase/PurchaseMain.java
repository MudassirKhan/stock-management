package com.stockmanagement.data.purchase;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.stockmanagement.R;
import com.stockmanagement.data.LoginActivity;
import com.stockmanagement.data.database.DatabaseHelper;
import com.stockmanagement.data.fragments.TabsAdapter;
import com.stockmanagement.data.utils.PreferenceUtils;

import static android.widget.Toast.LENGTH_SHORT;

public class PurchaseMain extends AppCompatActivity {

    TabLayout tabLayout;
    TabsAdapter tabsAdapter;
    ViewPager viewPager;

    ImageView menuitem ;
    PreferenceUtils pref;

    DatabaseHelper dbhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.purchase_activity);

        pref = new PreferenceUtils(this);
        dbhelper = new DatabaseHelper(this);


        tabLayout = findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Purchase"));
        tabLayout.addTab(tabLayout.newTab().setText("Sold"));
        tabLayout.addTab(tabLayout.newTab().setText("Net stock"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager =(ViewPager)findViewById(R.id.view_pager);
        menuitem =findViewById(R.id.menuitem);

        tabsAdapter = new TabsAdapter( getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(tabsAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewPager.setCurrentItem(tab.getPosition());

            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        menuitem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popup = new PopupMenu(PurchaseMain.this, menuitem);
                //inflating menu from xml resource
                popup.inflate(R.menu.option_menu);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {

                             case R.id.addProduct:

                                break;

                            case R.id.addCustomer:

                                break;

                            case R.id.logout:
                                pref.setLogin("0");
                                Intent intent=new Intent(PurchaseMain.this, LoginActivity.class);
                                startActivity(intent);
                                finish();
                                break;

                                /* case R.id.changepassword:
                              //  openDialouge(PurchaseMain.this);
                               // Toast.makeText(PurchaseMain.this, "Change password Not impmemented here", LENGTH_SHORT).show();
                                break;
*/


                        }
                        return false;
                    }
                });
                //displaying the popup
                popup.show();

                //will show popup menu here

            }
        });


    }

    private void openDialouge(final Activity context) {
        final EditText edtmobile, edtNewPass;
        Button btnChnage;

        final PreferenceUtils pref = new PreferenceUtils(context);
        final Dialog myDialog = new Dialog(context);
        myDialog.setContentView(R.layout.dialogue_password);
        myDialog.setTitle("Change Password");
        myDialog.setCancelable(true);
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        edtmobile = myDialog.findViewById(R.id.edtmobile);
        edtNewPass = myDialog.findViewById(R.id.edtNewPass);
        btnChnage = myDialog.findViewById(R.id.btnChange);

        btnChnage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    if (!edtmobile.getText().toString().equals("")) {
                        if (!edtNewPass.getText().toString().equals("")) {

                            Boolean resultupdate = dbhelper.updateChangePassword(edtmobile.getText().toString(),edtNewPass.getText().toString());

                            if (resultupdate == true) {
                                Toast.makeText(context, "password Changed Successfully", Toast.LENGTH_SHORT).show();
                                myDialog.dismiss();
                            }else {
                                Toast.makeText(context, "Please Try Again", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(context, "Please enter password", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, "Please Mobile Number", Toast.LENGTH_SHORT).show();
                    }

            }
        });

        myDialog.show();

    }
}
