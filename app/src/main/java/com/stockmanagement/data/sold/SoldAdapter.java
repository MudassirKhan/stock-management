package com.stockmanagement.data.sold;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.stockmanagement.R;
import com.stockmanagement.data.database.DatabaseHelper;
import com.stockmanagement.data.utils.PreferenceUtils;

import java.util.ArrayList;
import java.util.Collections;

public class SoldAdapter extends RecyclerView.Adapter<SoldAdapter.ViewHolder>  {

    Activity mContext;
    Context context;
    View itemLayoutView;
    ArrayList<ModelSold> arrayList =  new ArrayList<>();
    PreferenceUtils pref;
    DatabaseHelper db;

    public SoldAdapter(Activity mContext, ArrayList<ModelSold> arrayList) {
        this.mContext = mContext;
        this.context = context;
        this.arrayList = arrayList;
        pref = new PreferenceUtils(mContext);
        db = new DatabaseHelper(mContext);

    }

    @NonNull
    @Override
    public SoldAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.purchase_row, parent, false);
        SoldAdapter.ViewHolder viewHolder = new SoldAdapter.ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SoldAdapter.ViewHolder holder, int position) {

       // Collections.reverse(arrayList);
        holder.setdate.setText(arrayList.get(position).getP_date());
        holder.productName.setText(arrayList.get(position).getP_name());
        holder.customerName.setText(arrayList.get(position).getP_partyname());
        holder.weight.setText(arrayList.get(position).getP_weight());
        holder.kgorbags.setText(arrayList.get(position).getP_kgorbags());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView setdate , customerName,productName,weight,kgorbags;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            setdate = itemLayoutView.findViewById(R.id.setdate);
            customerName = itemLayoutView.findViewById(R.id.customerName);
            productName = itemLayoutView.findViewById(R.id.productName);
            weight = itemLayoutView.findViewById(R.id.weight);
            kgorbags = itemLayoutView.findViewById(R.id.kgorbags);
        }
    }
}
