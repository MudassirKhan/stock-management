package com.stockmanagement.data.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PreferenceUtils {

    SharedPreferences preferences;
    Context context;

    public PreferenceUtils(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        this.context = context;
    }

    public void setLogin(String loginkey) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("islogin", loginkey);
        editor.commit();
    }

    public String getLogin() {
        return preferences.getString("islogin", "");
    }
}
